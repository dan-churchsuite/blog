/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************!*\
  !*** ./resources/js/reorder.js ***!
  \*********************************/
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

window.postData = function () {
  return {
    dragging: null,
    dropping: null,
    over: null,
    getDropzone: function getDropzone(post) {
      post.id = 'dropzone';
      return post;
    },
    drop: function drop() {
      var a = _toConsumableArray(this.$store.posts.posts);

      if (this.dragging !== null && this.dropping !== null) {
        if (this.dragging < this.dropping) {
          a = [].concat(_toConsumableArray(a.slice(0, this.dragging)), _toConsumableArray(a.slice(this.dragging + 1, this.dropping + 1)), [a[this.dragging]], _toConsumableArray(a.slice(this.dropping + 1)));
        } else {
          a = [].concat(_toConsumableArray(a.slice(0, this.dropping)), [a[this.dragging]], _toConsumableArray(a.slice(this.dropping, this.dragging)), _toConsumableArray(a.slice(this.dragging + 1)));
        }

        ;
        this.dropping = null;
      }

      this.$store.posts.posts = a;
    }
  };
};
/******/ })()
;