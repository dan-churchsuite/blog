window.postData = function() {
    return {
        dragging: null,
        dropping: null,
        over: null,
        getDropzone(post) {
            post.id = 'dropzone';
            return post;
        },
        drop() {
            let a = [...this.$store.posts.posts];
            if (this.dragging !== null && this.dropping !== null) {
               if (this.dragging < this.dropping) {
                   a = [...a.slice(0, this.dragging), ...a.slice(this.dragging + 1, this.dropping + 1), a[this.dragging], ...a.slice(this.dropping + 1)];
            } else {
                   a = [...a.slice(0, this.dropping), a[this.dragging], ...a.slice(this.dropping, this.dragging), ...a.slice(this.dragging + 1)] }; this.dropping = null;
            }
            this.$store.posts.posts = a;
        },
    }
}