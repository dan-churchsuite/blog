<x-layout>
    <form class="flex flex-col space-y-3 max-w-2xl mx-auto" action="/blog" method="POST" enctype="multipart/form-data"> 
        @csrf
        <input class="shadow-lg rounded w-full p-2" type="text" name="title" placeholder="Title..." value="{{ old('title') }}">
        @error('title')
            <x-validation_error :message="$message"/>
        @enderror
        <textarea class="p-2 shadow-lg rounded w-full" name="body" placeholder="Body..." value="{{ old('body') }}"></textarea>
        @error('body')
            <x-validation_error :message="$message"/>
        @enderror
        <select class="p-2 rounded shadow-lg" name="category">
            <option class="text-gray-400" value="1" selected>Choose a category</option>
            @foreach ($categories as $category)
                <option value="{{ $category->id }}" {{ old('category') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
            @endforeach
        </select>  
        @error('category')
            <x-validation_error :message="$message"/>
        @enderror
       {{-- <input class="shadow-lg" type="file" name="image">
        @error('file')
            <x-validation_error :message="$message"/>
        @enderror --}}
        <button class="border rounded bg-purple-400 w-min py-2 px-4 inline-block self-center text-gray-50 hover:bg-purple-500" type="submit">Submit</button>
    </form>
</x-layout>
