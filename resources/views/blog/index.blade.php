<x-layout>
  <x-slot name="sidebar">
    <x-sidebar :categories="$categories"/>
  </x-slot>
  <div
        x-data="postData()"
        x-init="$store.posts.getPosts()"
    >
      <div
          class=""
          x-ref="container"
          x-on:dragover.prevent="$event.dataTransfer.dropEffect = 'move'"
          x-on:drop.prevent="drop()"
      >
          <template x-for="(post, index) in $store.posts.posts" :key="post">
              <div class="bg-gray-50">
                  <div
                      class="mb-4"
                      :class="{
                          'opacity-50': dragging == index,
                          'bg-blue-50': dropping == index,
                      }"
                      draggable="true"
                      x-on:dragstart="dragging=index"
                      x-on:dragend="dragging= null"
                      x-on:dragenter.prevent="if(index !== dragging) {dropping = index}"
                      x-on:dragleave="if(dropping === index) dropping = null"
                  >
                      <span class="" x-html="post"></span>
                  </div>
              </div>
          </template>
      </div>
  </div>
</x-layout>
