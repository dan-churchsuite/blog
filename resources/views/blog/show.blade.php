<x-layout>
 <article class="flex w-full rounded">
    <!-- image -->
    <!-- blog preview -->
    <section class="p-4">
        <a class="inline-block cursor-pointer">
            <h3 class="text-lg text-white text-purple-500 hover:text-purple-400 px-4 mb-1"><a href="/blog/{{ $post->slug }}">{{ $post->title }}</a></h3>
        </a>
        <div class="px-4">
            <small class="text-gray-600">{{ $post->created_at->format('M jS Y') }}
                @if ($post->category->name)
                    in <a class="hover:text-purple-500" href="/filter/{{ $post->category->id }}">{{$post->category->name}}</a>
                @endif
            </small>
            <div class="pt-1">{{ Str::words($post->body, 20, '...') }}</div>
            by {{ $post->user->name }}
            {{ $post->category->name }}
        </div>
    </section>
</article>
</x-layout>
