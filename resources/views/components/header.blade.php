
    <div class="{{ $site_width }} w-full flex justify-between items-center px-4 py-4">
         <a href={{ route('home') }}>
            <div class="flex items-center">
                <div class="w-12 h-12 transform transition duration-150 hover:rotate-6 hover:scale-105 transition" style="background-image: url({{ url('/images/w.png') }}); background-size: contain; background-repeat: no-repeat;"></div>
               <span class="text-3xl font-thin py-2 border-b border-transparent transition duration-300 hover:border-red-400">Watts Happening</span>
            </div>
        </a>
        <div class="">
            <ul class="flex space-x-3">
                <li class="border-b border-transparent transition duration-300 hover:border-red-400 py-2">
                    <a href="#">Resources</a>
                </li>
                <li class="border-b border-transparent transition duration-300 hover:border-red-400 py-2">
                    @guest
                        {{-- <a href={{ route('login') }}>Login</a> --}}
                    @endguest
                    @auth
                        <form action={{ route('logout') }} method="POST"> 
                            @csrf 
                            <button type="submit">Logout</button>
                        </form>
                    @endauth
                </li>
            </ul>
        </div>
    </div>
