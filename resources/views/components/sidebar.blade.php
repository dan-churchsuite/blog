<div class="w-72 hidden md:block mr-4">
    <div class="pb-6 pt-4">
        <h2 class="text-xl font-thin pb-2">Categories</h2>
        <ul class="flex space-x-3">
        @foreach ($categories as $category)
            <li x-data>
                <span class="hover:text-purple-500 hover:border-b" x-on:click="$store.posts.getPosts('/api/filter/{{ $category->id }}'); console.log($store.posts)">{{ $category->name }}</span>
            </li>
        @endforeach
            <li x-data>
                <span class="hover:text-purple-500 hover:border-b" x-on:click="$store.posts.getPosts('/api/blog'); console.log($store.posts)">All</span>
            </li>
        </ul>
    </div>
    <hr>
     <div class="py-6">
        <h2 class="text-xl font-thin pb-2">Social</h2>
         <ul class="flex space-x-3">
            <x-button chunky=true label="Github" icon="fab fa-github" class="border"/>
            <x-button chunky=true label="Facebook" icon="fab fa-facebook" class="border"/>
        </ul>
    </div>
    <hr>
</div>