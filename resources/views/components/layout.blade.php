<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.gstatic.com">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/reorder.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/@ryangjchandler/spruce@2.x.x/dist/spruce.umd.js"></script>
    <script src="https://kit.fontawesome.com/5fcb685337.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <style type="text/css">
        [x-cloak] {
            display: none !important;
        }
    </style>
</head>
<body class="h-screen flex flex-col justify-between text-gray-800 bg-{{ $site_neutral_col }}" style="font-family: 'Open Sans', sans-serif;">
    <header class="w-full flex justify-center"><x-header/></header>
    <main class="w-full h-full">
        @auth
            <header class="bg-gray-200 py-2">
                <div class="{{ $site_width }} w-full m-auto px-4 flex justify-between">
                    <div class="flex space-x-3">
                        <x-button
                            label="Go to dashboard"
                            href="/blog"
                        />
                        <x-button
                            label="Create a post"
                            href="/blog/create"
                            class='mr-auto'
                        />
                    </div>
                    <x-button
                        x-data=""
                        x-on:click="$store.admin_controls.open = !$store.admin_controls.open"
                        x-on:click.away="$store.admin_controls.open = false"
                        label="Quick Edit"
                    />
                </div>
            </header>
        @endauth
        <section class="flex {{ $site_width }} w-full m-auto px-4 my-4">
            <div>{{ $sidebar ?? ''}}</div>
            <div class="flex-grow px-4">{{ $slot }}</div>
        </section>
    </main>
    <footer class="border-t w-full flex justify-center h-32"><x-footer/></footer>
    <script>
        window.Spruce.store('admin_controls', {
            open: false,
        });
         window.Spruce.store('posts', {
            posts: [],
            query: '/api/blog',
            getPosts(query = null) {
                this.posts = [];
                fetch(query ?? this.query)
                .then(response => response.json())
                .then(data => {
                    data.posts.forEach((post) => {
                        this.posts.push(post);
                    })
                });
            },
        });
    </script>
</body>
</html>