<article x-data="" class="flex w-full shadow-lg mb-4">
    <!-- image -->
    <!-- blog preview -->
    <section class="my-4 relative">
        <div :class="$store.admin_controls.open ? 'opacity-25 bg-gray-200' : ''">
            <h3 class="text-lg text-white text-purple-500 hover:text-purple-400 px-4 mb-1"><a href="/blog/{{ $post->slug }}">{{ $post->title }}</a></h3>
            <div class="px-4">
                <small class="text-gray-600">{{ $post->created_at->format('M jS Y') }}
                    @if ($post->category)
                        in <a class="hover:text-purple-500" href="/filter/{{ $post->category->id }}">{{$post->category->name}}</a>
                    @endif
                </small>
                <div class="pt-1"><span>{{ Str::words($post->body, 28, '...') }}</span><small><a class="ml-2 text-gray-400 hover:text-purple-500" href="/blog/{{ $post->slug }}">Keep reading</a></small></div>
            </div>
        </div>
        <div class="z-10 h-full w-full absolute inset-0 flex space-x-3  justify-end items-center p-4" x-show="$store.admin_controls.open">
            <x-button x-data="" x-cloak="" x-on:click="console.log($el)" label="Edit"/>
            <x-button x-data="" x-cloak="" x-on:click="console.log($el)" label="Delete" color="red"/>
        </div>
    </section>
</article>