<button
    {{ $attributes->class([
        'py-3' => $chunky,
        'py-1' => !$chunky,
        'px-4 rounded bg-white shadow-lg',
        $get_color,
    ]) }}
    type="{{ $type }}"
>
    {!! $get_icon !!}
    @if ($href)
        <a href="{{ $href }}">{{ $label }}</a>
    @else
        {{ $label }}
    @endif
</button>
