<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{
    Post,
    Category,
};
use App\Services\{
    SlugService,
    ImageHandlerService,
};
use PhpParser\Node\Stmt\TryCatch;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('blog.index')
            ->with('posts', Post::orderBy('updated_at', 'DESC')
                ->with(['category', 'user'])->get())
            ->with('categories', Category::has('post')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create')
            ->with('categories', Category::all());;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            // 'image' => 'required|mimes:jpg,png,jpeg|max:5048'
        ]);

        Post::create([
            'title' => $request->input('title'),
            'body' => $request->input('body'),
            // 'image_path' => $this->imageHandlerService::handle($request),
            'slug' => $this->slugService::slugify($request->title),
            'user_id' => auth()->user()->id,
            'category_id' => $request->input('category')
        ]);

        redirect('/')->with('message', 'Post saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $blog) {
        return view('blog.show')
            ->with('post', $blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct(ImageHandlerService $imageHandlerService, SlugService $slugService)
    {
        $this->imageHandlerService = $imageHandlerService;
        $this->slugService = $slugService;
    }
}
