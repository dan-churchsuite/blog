<?php 
namespace App\Services;

use Illuminate\Support\Str;

class SlugService {

    public static function slugify($string): string {
        return str_replace(' ', '-', Str::words($string, 5));
    }

}