<?php 
namespace App\Services;

use Illuminate\Http\Request;

class ImageHandlerService {

    public static function handle(Request $request) {
        $new_image_name = uniqid().'-'.$request->title.'.'.$request->image->extension();
        $request->image->move(public_path('images'), $new_image_name);

        return $new_image_name;
        }

}