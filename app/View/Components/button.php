<?php

namespace App\View\Components;

use Illuminate\View\Component;

class button extends Component
{
    public $alpine;
    public $chunky;
    public $href;
    public $icon;
    public $label;
    public $type;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $color = '', bool $chunky = false, string $href = '', string $icon = '', string $type = 'button', string $label = 'Button')
    {
        $this->color = $color;
        $this->chunky = $chunky;
        $this->href = $href;
        $this->icon = $icon;
        $this->label = $label;
        $this->type = $type;
    }

    public function get_color() {
        if (!$this->color) return 'hover:bg-purple-500 text-gray-700 hover:text-gray-100';
        return 'text-gray-100 hover:bg-'.$this->color.'-500 bg-'.$this->color.'-400';
    }

    public function get_icon(): ?string {
        return $this->icon ? '<i class="'.$this->icon.'"></i>' : null;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.button');
    }
}
