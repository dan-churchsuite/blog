<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Post;
use App\Models\Category;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/blog', function() {
    $posts = Post::with(['category', 'user'])->get()->map(function($post) {
        return (string)view('components.card')->with('post', $post);
    });

    return response()->json([
        'posts' => $posts,
    ]);
});

Route::get('/filter/{id}', function ($id) {
    $posts = Post::with(['category', 'user'])
        ->where('category_id', $id)
        ->get()
        ->map(function ($post) {
        return (string)view('components.card')->with('post', $post);
    });

    return response()->json([
        'posts' => $posts,
    ]);
});
