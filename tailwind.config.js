module.exports = {
  purge: [
    './resources/views/**/*.blade.php',
    './resources/css/**/*.css',
  ],
  theme: {
    extend: {}
  },
  variants: {
    extend: {
      rotate: ['group-hover'],
    }
  },
  plugins: [
    require('@tailwindcss/ui'),
  ]
}
